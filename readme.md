# Giraffe experience 

Real estate application with 360 property view.

## Testing:

Open the attached index.html file. Enjoy.

## Working functionality:

1. Overlays for surfaces (see mountain image)
2. Clickable pins.
3. Actions on click of a pin.
3. Basic touch screen capabilities.
3. Pins with text.

## Implementation changes

Due to several problems with Google Street View, it would be much wiser if we use [Photo-Sphere-Viewer](https://github.com/mistic100/Photo-Sphere-Viewer) as the basic library behind our concept.

## Team members

* Amir
* Liga
* Liina
* Maciej
